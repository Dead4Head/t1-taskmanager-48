package ru.t1.amsmirnov.taskmanager.dto.request.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class TaskCompleteByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskCompleteByIdRequest() {
    }

    public TaskCompleteByIdRequest(
            @Nullable final String token,
            @Nullable final String id
    ) {
        super(token);
        setId(id);
    }

    @Nullable
    public String getId() {
        return id;
    }

    public void setId(@Nullable final String id) {
        this.id = id;
    }

}
