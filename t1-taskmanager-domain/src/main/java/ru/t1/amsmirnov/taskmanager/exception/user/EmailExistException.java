package ru.t1.amsmirnov.taskmanager.exception.user;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public final class EmailExistException extends AbstractException {

    public EmailExistException() {
        super("Error! This email is already used...");
    }

}
