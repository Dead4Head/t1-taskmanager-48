package ru.t1.amsmirnov.taskmanager.api.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    EntityManager getEntityManager();

    @NotNull
    EntityManagerFactory getEntityManagerFactory();

    @NotNull
    Liquibase getLiquibase() throws Exception;

    void close();

}
