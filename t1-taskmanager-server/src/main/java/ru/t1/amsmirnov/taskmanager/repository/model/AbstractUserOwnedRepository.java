package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.repository.model.IAbstractUserOwnedRepository;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M>
        implements IAbstractUserOwnedRepository<M> {


    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}
