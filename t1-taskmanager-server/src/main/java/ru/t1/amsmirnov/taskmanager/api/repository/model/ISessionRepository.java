package ru.t1.amsmirnov.taskmanager.api.repository.model;

import ru.t1.amsmirnov.taskmanager.model.Session;

public interface ISessionRepository extends IAbstractUserOwnedRepository<Session> {
}
