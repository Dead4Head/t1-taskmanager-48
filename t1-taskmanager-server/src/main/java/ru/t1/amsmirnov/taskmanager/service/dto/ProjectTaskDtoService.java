package ru.t1.amsmirnov.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.ITaskDtoRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IProjectDtoService;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IProjectTaskDtoService;
import ru.t1.amsmirnov.taskmanager.api.service.model.IProjectTaskService;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ProjectNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.TaskNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.ProjectIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.TaskIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;


public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskDtoService(
            @NotNull final IConnectionService connectionService
    ) {
        this.connectionService = connectionService;
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
        @NotNull final IProjectDtoService projectService = new ProjectDtoService(connectionService);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (!projectService.existById(userId, projectId)) throw new ProjectNotFoundException();
            final TaskDTO task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(task);
            transaction.commit();
        } catch (final Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
        @NotNull final IProjectDtoService projectService = new ProjectDtoService(connectionService);
        try {
            transaction.begin();
            if (!projectService.existById(userId, projectId)) throw new ProjectNotFoundException();
            final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, projectId);
            if (tasks == null) throw new ModelNotFoundException("TaskList");
            taskRepository.removeAll(userId, projectId);
            projectService.removeOneById(userId, projectId);
            transaction.commit();
        } catch (final Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
        @NotNull final IProjectDtoService projectService = new ProjectDtoService(connectionService);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (!projectService.existById(userId, projectId)) throw new ProjectNotFoundException();
            final TaskDTO task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            taskRepository.update(task);
            transaction.commit();
        } catch (final Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }
    }

}
